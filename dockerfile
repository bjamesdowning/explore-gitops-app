#FROM ubuntu:latest
#RUN useradd -u 1000 billy

FROM scratch
EXPOSE 8080
COPY gosql /
COPY templates/ templates
COPY static/ static
#COPY --from=0 /etc/passwd /etc/passwd
#USER 1000
CMD ["/gosql"]
