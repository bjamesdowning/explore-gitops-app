package main

import (
	"encoding/json"
	"fmt"
	"log"
	"net/http"
	"os"
	"strconv"
	"text/template"

	"gorm.io/driver/mysql"
	"gorm.io/gorm"
)

// Database Connection
// Add New Comments
var dsn = mysqlDB()
var db, _ = gorm.Open(mysql.Open(dsn), &gorm.Config{})
var tmpl *template.Template
var go_test_models []GoTestModel

func init() {
	tmpl = template.Must(template.ParseGlob("templates/*.html"))
}

type GoTestModel struct {
	Name int
	Year int
}

func main() {
	http.Handle("/static/", http.StripPrefix("/static/", http.FileServer(http.Dir("static"))))
	http.HandleFunc("/", goDatabaseCreate)
	http.HandleFunc("/results", results)
	log.Fatal(http.ListenAndServe(":8080", nil))
}

func goDatabaseCreate(w http.ResponseWriter, r *http.Request) {
	tmpl.ExecuteTemplate(w, "index.html", nil)
}

func results(w http.ResponseWriter, r *http.Request) {
	tmpl.ExecuteTemplate(w, "results.html", nil)
	uname := r.FormValue("uname")
	pword := r.FormValue("pword")
	unameint, _ := strconv.Atoi(uname)
	pwordint, _ := strconv.Atoi(pword)
	GoTestModel := GoTestModel{
		Name: unameint,
		Year: pwordint}

	db.Create(&GoTestModel)
	if err := db.Create(&GoTestModel).Error; err != nil {
		log.Fatalln((err))
	}
	result := db.Find(&go_test_models)
	json.NewEncoder(w).Encode((&GoTestModel))
	fmt.Fprint(w, result.RowsAffected)

	fmt.Println("Fields Added", GoTestModel)
}

func mysqlDB() string {
	host := os.Getenv("MYSQL_HN")
	if len(host) == 0 {
		host = "localhost"
	}
	port := os.Getenv("MYSQL_PORT")
	if len(port) == 0 {
		port = "3306"
	}

	username := os.Getenv("MYSQL_USERNAME")
	if len(username) == 0 {
		username = "root"
	}

	password := os.Getenv("MYSQL_PASSWORD")
	if len(password) == 0 {
		password = "invalid"
	}

	database := os.Getenv("MYSQL_DB")
	if len(database) == 0 {
		database = "go_test"
	}
	return username + ":" + password + "@tcp(" + host + ":" + port + ")/" + database + "?charset=utf8mb4"
	// root:example@tcp(localhost:3306)/go_test?charset=utf8mb4
}
